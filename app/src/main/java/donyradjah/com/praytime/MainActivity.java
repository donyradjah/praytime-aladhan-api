package donyradjah.com.praytime;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    TextView txtLat;
    ListView lv;

    // URL to get contacts JSON
    String url = "http://hackathon.kodesoft.co.id/api/v1/publik/find-provinsi?limit=1000";
    private ProgressDialog pDialog;
    private static final String TAG_DATA = "data";
    private static final String TAG_TIMINGS = "timings";
    private static final String TAG_DATE = "date";
    private static final String TAG_SUBUH = "Fajr";
    private static final String TAG_SUNRISE = "Sunrise";
    private static final String TAG_DHUHUR = "Dhuhr";
    private static final String TAG_ASHAR = "Asr";
    private static final String TAG_SUNSET = "Sunset";
    private static final String TAG_MAGHRIB = "Maghrib";
    private static final String TAG_ISHA = "Isha";
    private static final String TAG_TANGGAL = "readable";
    JSONArray jadwals = null;
    ArrayList<HashMap<String, String>> jadwalList;
    public GPSTracker gps = new GPSTracker(MainActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gps = new GPSTracker(MainActivity.this);
        jadwalList = new ArrayList<HashMap<String, String>>();
        lv = (ListView) findViewById(R.id.list);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            Calendar c = Calendar.getInstance();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            url = "http://api.aladhan.com/calendar?latitude=" + latitude + "&longitude=" + longitude + "&timezonestring=" + TimeZone.getDefault().getID() + "&method=2&month=" + (c.get(Calendar.MONTH) + 1) + "&year=" + c.get(Calendar.YEAR);


        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();

        }
        new sendRequest().execute();
    }

    private class sendRequest extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();


        }

        protected Void doInBackground(Void... arg0) {
            Calendar cal = Calendar.getInstance();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            url = "http://api.aladhan.com/calendar?latitude=" + latitude + "&longitude=" + longitude + "&timezonestring=" + TimeZone.getDefault().getID() + "&method=2&month=" + (cal.get(Calendar.MONTH) + 1) + "&year=" + cal.get(Calendar.YEAR);
Log.e("url",url);
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    jadwals = jsonObj.getJSONArray(TAG_DATA);

//                                     looping through All Contacts
                    for (int i = 0; i < jadwals.length(); i++) {
                        JSONObject c = jadwals.getJSONObject(i);


                        JSONObject timings = c.getJSONObject(TAG_TIMINGS);
                        JSONObject date = c.getJSONObject(TAG_DATE);
                        String tanggal = date.getString(TAG_TANGGAL);
                        String subuh = timings.getString(TAG_SUBUH);
                        String sunrise = timings.getString(TAG_SUNRISE);
                        String dhuhur = timings.getString(TAG_DHUHUR);
                        String ashar = timings.getString(TAG_ASHAR);
                        String sunset = timings.getString(TAG_SUNSET);
                        String maghrib = timings.getString(TAG_MAGHRIB);
                        String isya = timings.getString(TAG_ISHA);


                        // tmp hashmap for single contact
                        HashMap<String, String> jadwal = new HashMap<String, String>();


//                                        // adding each child node to HashMap key => value
                                        jadwal.put(TAG_SUBUH, subuh);
                                        jadwal.put(TAG_SUNRISE, sunrise);
                                        jadwal.put(TAG_DHUHUR, dhuhur);
                                        jadwal.put(TAG_ASHAR, ashar);
                                        jadwal.put(TAG_SUNSET, sunset);
                                        jadwal.put(TAG_MAGHRIB, maghrib);
                                        jadwal.put(TAG_ISHA, isya);
                                        jadwal.put(TAG_TANGGAL, tanggal);
//
//                                        // adding contact to contact list
                                        jadwalList.add(jadwal);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    MainActivity.this, jadwalList,
                    R.layout.item, new String[]{
                    TAG_TANGGAL,TAG_SUBUH, TAG_SUNRISE, TAG_DHUHUR, TAG_ASHAR, TAG_SUNSET, TAG_MAGHRIB,TAG_ISHA}, new int[]{
                     R.id.tanggal, R.id.subuh, R.id.sunrise, R.id.dhuhur, R.id.ashar, R.id.sunset, R.id.maghrib, R.id.isya});

            lv.setAdapter(adapter);
        }
    }

}

