package donyradjah.com.praytime;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by cvgs on 5/25/2016.
 */
public class SquareImageVIew extends ImageView {
    public SquareImageVIew(Context context) {
        super(context);
    }

    public SquareImageVIew(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageVIew(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}