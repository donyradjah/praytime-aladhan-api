package donyradjah.com.praytime;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cvgs on 5/25/2016.
 */
public final class MyAdapter extends BaseAdapter {
    private final List<Item> mItems = new ArrayList<Item>();
    private final LayoutInflater mInflater;

    public MyAdapter(Context context) {
        mInflater = LayoutInflater.from(context);

        mItems.add(new Item("jadwal",       R.drawable.jadwalsholat));
        mItems.add(new Item("Magenta",   R.drawable.transparent));
        mItems.add(new Item("tuntunnan", R.drawable.tuntunan));
        mItems.add(new Item("Gray",      R.drawable.transparent));
        mItems.add(new Item("harini",     R.drawable.harini));
        mItems.add(new Item("Cyan",      R.drawable.transparent));
        mItems.add(new Item("setlokasi",       R.drawable.setlokasi));
        mItems.add(new Item("Magenta",   R.drawable.transparent));
        mItems.add(new Item("bantuan", R.drawable.bantuan));
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Item getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mItems.get(i).drawableId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        final TextView name;

        if (v == null) {
            v = mInflater.inflate(R.layout.grid_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.text, v.findViewById(R.id.text));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.text);

        final Item item = getItem(i);
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Log.e("id",item.name);
            }
        });
        picture.setImageResource(item.drawableId);
        name.setText(item.name);


        return v;
    }

    private static class Item {
        public final String name;
        public final int drawableId;

        Item(String name, int drawableId) {
            this.name = name;
            this.drawableId = drawableId;
        }
    }
}
